/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */
#ifndef simple_h
#define simple_h


class not_wrapped
{
public:
  inline not_wrapped() {}
};

class found_class
{
public:
  inline found_class() {}
};

class missing_base
  : public not_wrapped
{
public:
  inline missing_base() {}
  char val1_var;
};

#endif

