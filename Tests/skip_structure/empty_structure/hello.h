// Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
// file Copyright.txt

#ifndef HELLO_H
#define HELLO_H
#include <string>

namespace outer{
    namespace simple{
        std::string hello();
    }
}

#endif // HELLO_H
