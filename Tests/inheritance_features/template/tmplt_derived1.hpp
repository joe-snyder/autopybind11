/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */

#ifndef TMPLT_DERIVED1_HPP
#define TMPLT_DERIVED1_HPP

#include "non_template/base.hpp"
#include <string>

namespace nmspc1 {

// Short for templated derived 1
template <typename T>
class TD1 : public Base
{
public:
  T var1;

  inline TD1() { var1 = T(0); }
  inline virtual std::string whoami() const override
  {
    return std::string("TD1");
  }

  inline virtual std::string virt1(float f) const
  {
    return std::string("TD1.virt1()");
  }

protected:
  virtual std::string prot_virt_fxn() const override
  {
    return std::string("TD1.prot_virt_fxn()");
  }

private:
  // Make sure private non-pure virtual functions aren't included in trampoline
  inline virtual std::string priv_virt_fxn() const override
  {
    return std::string("TD1.priv_virt_fxn()");
  }
};

template <typename T>
inline std::string call_virt_from_td1(const TD1<T>& td1)
{
  std::string ret;
  ret += td1.whoami();
  ret += ": ";
  ret += td1.virt1(3.14);
  return ret;
}
}
#endif