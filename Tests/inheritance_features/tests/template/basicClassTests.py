# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import unittest
import sys
import os

# Make it so we search where we are running.
sys.path.append(os.getcwd())

import inheritance_module as im
from inheritance_module import nmspc1
from inheritance_module.nmspc1 import nmspc2

# Not going to test all of the same functionality as in non_template/basicClassTests,
# as basic class functionality has been tested thoroughly elsewhere in the examples.
# Just test new functionality not tested elsewhere
class basicClassBehavior(unittest.TestCase):
    # Should be "public" on the Python side. Implemented in Base class
    def test_prot_inherited_mthds(self):
        exp_string = "Base.prot_fxn()"
        self.assertEqual(nmspc1.TAD1_float().prot_fxn(), exp_string)
        self.assertEqual(nmspc1.TAD1_int().prot_fxn(), exp_string)
        self.assertEqual(nmspc1.TD1_float().prot_fxn(), exp_string)
        self.assertEqual(nmspc1.TD1_int().prot_fxn(), exp_string)
        self.assertEqual(nmspc2.TD2_float_double().prot_fxn(), exp_string)
        self.assertEqual(nmspc2.TD2_int_double().prot_fxn(), exp_string)


if __name__ == "__main__":
    unittest.main()
