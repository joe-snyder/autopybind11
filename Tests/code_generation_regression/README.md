# Smoke Test

This test is meant to test the Python-only API of `autopyinbd11`.

It does not check that the files can be compiled, as it does not rely on any
CMake instrumentation.

See root-level README about running this independently.
