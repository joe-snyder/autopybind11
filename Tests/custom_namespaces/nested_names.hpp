/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */
#ifndef NESTED_NAMESPACE_HPP
#define NESTED_NAMESPACE_HPP
namespace outer
{
    namespace inbetween {
        namespace outer_but_inner {
            class outer_but_inner_class
            {
                public:
                    inline outer_but_inner_class(){
                        var1 = 0;
                        var2 = 0;
                    }

                    inline void decrement_vars( int update1, double update2 )
                    {
                        var1 -= update1;
                        var2 -= update2;
                    }

                    inline void increment_vars( int update, double update2 )
                    {
                        var1 += update;
                        var2 += update2;
                    }
                    inline int sum_vars()
                    {
                        return var1 + (int)var2;
                    }
                private:
                    int var1;
                    double var2;
            };
        }
        class inbetweener
        {
            public:
                inline inbetweener(){
                    var1 = 0;
                    var2 = 0;
                }

                inline void decrement_vars( int update1, double update2 )
                {
                    var1 -= update1;
                    var2 -= update2;
                }

                inline void increment_vars( int update, double update2 )
                {
                    var1 += update;
                    var2 += update2;
                }
                inline int sum_vars()
                {
                    return var1 + (int)var2;
                }
            private:
                int var1;
                double var2;
        };
        inline int inbetweener_fun(int val1, double val2)
        {
            return val1 + val2;
        }
        namespace justafunc
        {
            inline int getvalue(int value)
            {
                return value;
            }
        }
        namespace three
        {
            class third_nmspc_class
            {
            public:
                inline third_nmspc_class(){
                    var1 = 0;
                    var2 = 0;
                }

                inline void decrement_vars( int update1, double update2 )
                {
                    var1 -= update1;
                    var2 -= update2;
                }

                inline void increment_vars( int update, double update2 )
                {
                    var1 += update;
                    var2 += update2;
                }
                inline int sum_vars()
                {
                    return var1 + (int)var2;
                }
            private:
                int var1;
                double var2;
            };

            inline int third_namespace_fun(int val1, int val2)
            {
                return val1 + val2;
            }
        }
    }

    inline int outer_namespace_fun(int val1, int val2)
    {
        return val1 + val2;
    }
}
#endif // NESTED_NAMESPACE_HPP