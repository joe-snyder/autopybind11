/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */
#ifndef SECOND_NAMESPACE_HPP
#define SECOND_NAMESPACE_HPP
namespace second
{
  template < typename S=float, typename T=double >
  class nmspc2_class
  {
  public:
    S var1;
    T var2;

    inline nmspc2_class()
    {
      var1 = S( 0 );
      var2 = T( 0 );
    }

    inline void update_vars( S update1, T update2 )
    {
      var1 += update1;
      var2 += update2;
    }

    inline void update_vars( S update )
    {
      var1 += update;
      var2 += T( update );
    }
  };

  template < typename S, typename T >
  T nmspc2_func( S val1, T val2 )
  {
    return T( val1 + val2 );
  }
}
#endif